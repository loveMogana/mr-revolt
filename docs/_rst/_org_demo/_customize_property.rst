==================
Customize Property
==================

    :Author: revolt
    :Date: <2019-11-30 Sat 14:00>

;;-**- mode:org; -**-

1 Customize Property
--------------------

1.1 Case Insensitive property
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    *** CD collection 
    **** classic
    ***** goldberg Variations 
    :PROPERTIES: 
    :Title:    Goldberg Variations
    :Composer: J.S. Bach
    :Artist:   Glenn Gould
    :Publisher: Deutsche Grammophon
    :NDisks:   1
    :END 

1.2 pre-defined properties
~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    ***  CD collection 
    :PROPERTIES:
    :NDisks_ALL: 1 2 3 4
    :Publisher_ALL: "Deutshce Grammophon" Philips EMI
    :END:

1.3 one property has more values
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    *** CD collection
    **** Classic
    :PROPERTIES:
    :Genres:   Classic
    :END:
    ***** Goldberg Variables
    :PROPERTIES:
    :Title:    Goldberg Variations
    :Composer: J.S. Bach
    :Artist:   Glenn Gould
    :Publisher: Deutsche Grammophon
    :NDisks:   1
    :Genres+:  Baroqus
    :END:
    ***  Set a column view
    **** Top node for columns view 
    :PROPERTIES:
    :COLUMNS:  %25ITEM %TAGS %PRIORITY %TODO
    :END:
    ***** subtree
    I will learn about column view knowledge. 

1.4 One column definition
~~~~~~~~~~~~~~~~~~~~~~~~~

::

    :PROPERTIES:
    :COLUMNS:  %25ITEM %9Approved(Approved?){X} %Owner %11Status %10Time_Estimate{:} %CLOCKSUM %CLOCKSUM_T
    :Owner_ALL: Tammy Mark Karl Lisa Don
    :Status_ALL: "In progress" "Not started yet" "Finished" ""
    :APPROVED_ALL: "[X]" "[ ]"
    :APPROVED: [X]
    :OWNER:    Tammy
    :STATUS:   In progress
    :TIME_ESTIMATE: 1:5
    :CLOCKSUM_ALL:
    :CLOCKSUM_T_ALL:
    :END:

The first column, ``'%25ITEM"``, means the first 25 characters of the
item itself.i.e.,of the headline.You probably always should start
the column definition with ``'ITEM'`` specifier.The other specifiers
create columns ``'Owener'`` with a list of  names as allowed values,for
``'status'`` with four different possible values,and for a checkbox
field ``'Approved'``.When no width is given after the ``'%'`` character,
the column is exactly as wide as it need to be in order to fully
display all values.The ``Approved`` column does have a modified title (
``'Approved?'``, with a question mark). Summaries are created for the
``'Time_Estimate'`` column by adding time duration expresions like
HH:MM,and for the ``'Approved'`` column,by providing an ``'[X]'`` status if
all children have been checked.The ``COLOCKSUM`` and ``CLOCKSUM_T`` columns
are special, they lists the sums of CLOCK intervals in the
subtree,either for all colocks or just for today.

you know,The peoperties ``CLOCKWUM`` means of the sum of ``CLOCK``
intervals in the subtree. ``org-clock-sum`` must be run first to
compute the values in the current buffer.

The properties ``COLOCK_T`` means of the sum of ``CLOCk`` intervals in the
subtree for today. ``org-clock-sum-today`` must be run first to
compute the values in the current buffer.

so we need to know what is ``Clocking work time``.

`Clocking work time reference link <https://orgmode.org/manual/Clocking-work-time.html>`_

1.5 Clock project time
~~~~~~~~~~~~~~~~~~~~~~

::

    :RevoltLogBook:
    CLOCK: [2019-11-16 六 22:44]--[2019-11-16 六 22:44] =>  0:00
    CLOCK: [2019-11-16 六 21:15]--[2019-11-16 六 21:22] =>  0:07
    CLOCK: [2019-11-16 六 21:10]--[2019-11-16 六 21:15] =>  0:05
    :END:

::

    #+BEGIN: clocktable :maxlevel 2 :emphasize t :block today :scope tree1 :link t :compact t
    #+CAPTION: Clock summary at [2019-11-30 Sat 12:16], for Saturday, November 30, 2019.

.. table:: Clock summary at [2019-11-30 Sat 12:16], for Saturday, November 30, 2019.

    +---------------------------------------------------------------------------------------------+----------+
    | Headline                                                                                    | Time     |
    +=============================================================================================+==========+
    | **Total time**                                                                              | **0:08** |
    +---------------------------------------------------------------------------------------------+----------+
    | **`Customize Property </home/revolt/org/revolt/docs/_org/_org_demo/_customize_property>`_** | **0:08** |
    +---------------------------------------------------------------------------------------------+----------+
    |   *`Columns view </home/revolt/org/revolt/docs/_org/_org_demo/_customize_property>`_*       | *0:08*   |
    +---------------------------------------------------------------------------------------------+----------+

- org-clock-in

- org-clock-out

- org-clock-in-last

- org-clock-modify-effort-estimate

- org-evaluate-time-range

- org-clock-cancel

- org-todo

- org-clock-goto

- org-clock-display

1.5.1 Org Clock Table
^^^^^^^^^^^^^^^^^^^^^

- org-colck-report

- org-dblock-update

- ``C-u C-c C-x C-u`` : update all clock table

  Defaults for all these options can be configured in the variable
  ``org-clocktable-defaults`` .

  for more,you could see `here <https://orgmode.org/manual/The-clock-table.html#The-clock-table>`_.

1.5.2 Resolving idle time and continuous clocking
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    :RevoltLogBook:
    CLOCK: [2019-11-17 日 10:20]--[2019-11-17 日 10:24] =>  0:04
    CLOCK: [2019-11-17 日 10:20]--[2019-11-17 日 10:20] =>  0:00
    :END:

If you clock in on a work item, and then walk away form your
computer - perhaps to take a phone call - you often need to
"resolve" the time you were away by either subtracting it from the
current clock,or applying it to another one.

By customizing the variable ``org-clock-idle-time`` to some
integer,such as 10 or 15,Emacs can alert you when you get back to
your computer after being idle for that many minutes,and ask what
you want to do with the idle time.There will be a question waiting
for you when you get back,indicating how much idle time has passed
constantly updated with the current amount,as well as a set of
choices to correct the discrepancy.

1.6 DONE Columns view
~~~~~~~~~~~~~~~~~~~~~

- State "DONE"       from              [2019-11-30 Sat 14:13]

::

    #+COLUMNS: %50ITEM %22SCHEDULED %22DEADLINE  %TODO %3PRIORITY %TAGS %ALLTAGS %FILE %TIMESTAMP

1.6.1 DONE Example of outline item with a SCHEDULE property
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

<2019-11-29 Fri 23:09>
CLOSED: [2019-11-30 Sat 12:20]  DEADLINE: <2019-11-29 Fri 19:00>  SCHEDULED: <2019-11-29 Fri 11:00>

- State "DONE"       from "WAITING"    [2019-11-30 Sat 12:20]

- State "WAITING"    from "TODO"       [2019-11-30 Sat 12:19]

::

    <2019-11-29 Fri 23:09>
    CLOSED: [2019-11-30 Sat 12:20] DEADLINE: <2019-11-29 Fri 19:00> SCHEDULED: <2019-11-29 Fri 11:00>
    :PROPERTIES:
    :END:
    :RevoltLogBook:
    - State "DONE"       from "WAITING"    [2019-11-30 Sat 12:20]
    - State "WAITING"    from "TODO"       [2019-11-30 Sat 12:19]
    :END:

The list of variable properties?Here it is:

.. table::

    +---------------------+----------------------------------------------------------+
    | Variable Properties | Description                                              |
    +=====================+==========================================================+
    | ITEM                | The content of the headline.                             |
    +---------------------+----------------------------------------------------------+
    | TODO                | The TODO keyword of the entry.                           |
    +---------------------+----------------------------------------------------------+
    | TAGS                | The tags defined directly in the headline.               |
    +---------------------+----------------------------------------------------------+
    | ALLTAGS             | All tags,including inherited ones.                       |
    +---------------------+----------------------------------------------------------+
    | PRIORITY            | The priority of the entry,a string with a single letter. |
    +---------------------+----------------------------------------------------------+
    | DEADLINE            | The deadline time string,without the angular brackets.   |
    +---------------------+----------------------------------------------------------+
    | SCHEDULED           | The scheduling time stamp,without the angular brackets.  |
    +---------------------+----------------------------------------------------------+

These are all ``special properties``,but of course you can define your
own properties.

Before going to the ranter complex stuff you can do with your own
properties,we'd like to know how to use different column views for
different subtrees.

1.6.2 Defining a column format for a subtree
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To define a column view for a specific item,just add the special
property: ``:COLUMNS:`` to it:

1.6.2.1 Top node for columns view
:::::::::::::::::::::::::::::::::

::

    :PROPERTIES:
    :COLUMNS:  %25ITEM %TAGS %PRIORITY %TODO
    :END:

This view will be used for the entry and its entire subtree - unless
some of its children has its own column view.

See for example this:

1.6.2.1.1 DONE Example1
'''''''''''''''''''''''

- State "DONE"       from "TODO"       [2019-11-30 Sat 12:20]

1.6.2.1.2 DONE Example2
'''''''''''''''''''''''

- State "DONE"       from "TODO"       [2019-11-30 Sat 12:20]

1.6.2.1.3 DONE Example3
'''''''''''''''''''''''

- State "DONE"       from "TODO"       [2019-11-30 Sat 12:20]

1.6.3 Adding summary types for some properties
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.6.3.1 My Project
::::::::::::::::::

::

    :PROPERTIES:
    :COLUMNS:  %20ITEM %9Approved(Approved?){X} %Owner %11Status %10Time_Spent{:}
    :Owner_ALL: LiuDong Zhangqi ZhangFan HaoFei Revolt
    :Status_ALL: "In progress" "Not started yet" "Finished" ""
    :APPROVED_ALL: "[X]" "[ ]"
    :END:

1.6.3.1.1 Item1
'''''''''''''''

::

    :PROPERTIES:
    :APPROVED: [X]
    :OWNER:    HaoFei
    :STATUS:   Finished
    :TIME_SPENT: 1:00
    :END:

1.6.3.1.2 Item2
'''''''''''''''

::

    :PROPERTIES:
    :APPROVED: [X]
    :OWNER:    LiuDong
    :STATUS:   In progress
    :TIME_SPENT: 0:15
    :END:

1.6.3.1.3 Item3
'''''''''''''''

::

    :PROPERTIES:
    :APPROVED: [X]
    :OWNER:    ZhangFan
    :STATUS:   Not started yet
    :END:

1.6.3.1.4 description this property
'''''''''''''''''''''''''''''''''''

We have a ``:COLUMNS:`` property,defining the column view.It says:

.. table::

    +---------------------------+------------------------------------------------+
    | Element                   | Description                                    |
    +===========================+================================================+
    | %20ITEM                   | display the item (20 characters for this field |
    +---------------------------+------------------------------------------------+
    | %9Approved(Approved?){X}  | display the "Approved" property                |
    +---------------------------+------------------------------------------------+
    | %Owner                    | display the "Owner" property                   |
    +---------------------------+------------------------------------------------+
    | %11Status                 | display the "Status" property                  |
    +---------------------------+------------------------------------------------+
    | %10Time\ :sub:`Spent`\{:} | display the "Time\ :sub:`Spent`\ property      |
    +---------------------------+------------------------------------------------+

The {x} and ``{:}`` mean:

- ``{X}`` means: display ``[X]`` if all entries have a ``[X]`` value for their
  "Approved" property ([-] or [] otherwise).

- ``{:}`` means: display a summary of the time spend,by adding all the
  values found in the property "Time\ :sub:`Spend`\".

  Once you get the ``:COLUMN:`` property defined,you can interactively add
  any property with ``C-c C-x p`` (org-set-property).It will prompt you for
  the name of the property,and offer default possible values depending
  on the ``_ALL`` friend of the property (if any)or on the values found in
  the buffer.

1.6.4 Defining all the possible values for a property
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Defining summary-types implies that you need to have a limited set of
possible values for certain properties.

For example,the ``"Approved"`` value discussed above should take only two
values: ``[ ]`` and ``[X]``.

Same for the "status" property:you might want to define only a few
status like "In Progress" "Not Started yet" "Finished".

You can restrict the allowed values for any property using the ``_ALL``
suffix like this: `1.6.3.1 My Project`_

Note: ``\*_ALL`` properties are meta-properties,defining rules on how to
use the properties themselves.

When you're in a field of the column,you can define all the possible
values for the associated property by pressing ``'a'``:it will prompt the
current set of allowed values and you will be able to edit it.

1.6.5 Complete example with three items in the subtree
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.6.5.1 My Project2
:::::::::::::::::::

::

    :PROPERTIES:
    :COLUMNS:  %20ITEM %9APPROVED(Approved?){X} %OWNER(Owner) %11STATUS(Status) %10Time_Spent{:}
    :STATUS_ALL: "In progress" "Not Started yet" "Finished" ""
    :OWNER_ALL: Tammy Mark Karl Lisa Don
    :APPROVED_ALL: "[ ]" "[X]"
    :END:

1.6.5.1.1 Item1
'''''''''''''''

::

    :PROPERTIES:
    :OWNER:    Tammy
    :STATUS:   Finished
    :TIME_SPENT: 1:45
    :APPROVED: [X]
    :END:

1.6.5.1.2 Item2
'''''''''''''''

::

    :PROPERTIES:
    :OWNER:    Don
    :STATUS:   In progress
    :TIME_SPENT: 0:15
    :APPROVED: [X]
    :END:

1.6.5.1.3 Item3
'''''''''''''''

::

    :PROPERTIES:
    :STATUS:   Not Started yet
    :OWNER:    Lisa
    :APPROVED: [ ]
    :END:

1.6.6 Editing properties from the column view
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The one great thing about the column view is that it lets you access
any edit any property very quickly.

See this: `1.6.3.1 My Project`_

- Use ``v`` to display the field value in the minibuffer.

- Use ``e`` to interactively select/edit the value.

- Use ``S-left/right`` to cycle through the allowed values in a filed.

- Use ``a`` to edit the allowed values for this property.

- Use ``S`` to edit one property all contents.

- Use ``r/s`` recreate the column view,to include recent change made in
  the buffer.

- Use ``<`` make the column narrower / wider by one character.

- Use ``S-M-LEFT`` Delete the current column.

- Use ``S-M-RIGHT`` Insert a new column.to the left of the current column.

1.6.7 DONE Case 2
^^^^^^^^^^^^^^^^^

- State "DONE"       from "TODO"       [2019-11-30 Sat 12:16]  
  well done realize clock time feature

::

    CLOSED: [2019-11-30 Sat 12:16] DEADLINE: <2019-11-30 Sat 12:00> SCHEDULED: <2019-11-30 Sat 11:09>

    :PROPERTIES:
    :COLUMNS:  %25ITEM %9APPROVED(Approved?){X} %Owner(Owner){$} %11STATUS(Status) %13TIME_ESTIMATE(Time_Estimate){:} %SCHEDULED %DEADLINE %CLOCKSUM %CLOCKSUM_T
    :APPROVED_ALL: "[ ]" "[X]"
    :APPROVED: [X]
    :STATUS_ALL: "In Progress" "Not Started yet" "Finished" ""
    :STATUS:   In Progress
    :OWNER_ALL: Tammy Mark Karl Lisa Don
    :OWNER:    Don
    :TIME_ESTIMATE_ALL: 30:40
    :TIME_ESTIMATE: 30:40
    :END:
    :RevoltLogBook:
    - State "DONE"       from "TODO"       [2019-11-30 Sat 12:16] \\
      well done realize clock time feature
    CLOCK: [2019-11-30 Sat 10:56]--[2019-11-30 Sat 10:57] =>  0:01
    :END:

1.6.8 Clock work time
^^^^^^^^^^^^^^^^^^^^^

`reference link <https://www.youtube.com/watch?v=uVv49htxuS8>`_

::

    #+SEQ_TODO: NEXT(n/!) TODO(t@/!) WAITING(w@/!) SOMEDAY(s/!) PROJ(p) | DONE(d@)
    #+TAGS: PHONE(o) COMPUTER(c) LUNCH(l) SHOPPING(s) FAMILY(f)
    #+ARCHIVE: myarchive.org::

1.6.8.1 GTD related things
::::::::::::::::::::::::::

1.6.8.2 Example
:::::::::::::::

1.6.8.2.1 Task A
''''''''''''''''

::

    :RevoltLogBook:
    CLOCK: [2019-11-30 Sat 11:50]--[2019-11-30 Sat 11:51] =>  0:01
    :END:

- some task that we need to know how much time it takes

1.6.8.2.2 Task B
''''''''''''''''

::

    :RevoltLogBook:
    CLOCK: [2019-11-30 Sat 11:58]--[2019-11-30 Sat 11:59] =>  0:01
    CLOCK: [2019-11-30 Sat 11:55]--[2019-11-30 Sat 11:57] =>  0:02
    :END:

- some other task to be clocked

1.6.8.3 Books
:::::::::::::

1.6.8.4 Legal obligations
:::::::::::::::::::::::::

1.6.8.5 Maintenance
:::::::::::::::::::

1.6.8.6 House stuff
:::::::::::::::::::

::

    :PROPERTIES:
    :Effort:   0:12
    :END:
    :RevoltLogBook:
    CLOCK: [2019-11-30 Sat 12:07]--[2019-11-30 Sat 12:08] =>  0:01
    CLOCK: [2019-11-30 Sat 12:04]--[2019-11-30 Sat 12:06] =>  0:02
    :END:
