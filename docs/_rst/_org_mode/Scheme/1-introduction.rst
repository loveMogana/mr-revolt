=====================
Scheme-1-Introduction
=====================

    :Author: lyps



1 DONE `Revised\ :sup:`6`\ Report on the Algorithmic Language Scheme <http://www.r6rs.org/final/html/r6rs/r6rs-Z-H-3.html#node_chap_Temp_3>`_ Added: ------ [2019-12-29 Sun 23:42]
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- State "DONE"       from "TODO"       [2019-12-30 Mon 02:05]

1.1 Introduction
~~~~~~~~~~~~~~~~

Programming languages should be designed not by piling feature on top
of feature, but by removing the weaknesses and restrictions that make
additional features appear necessary. Scheme demonstrates that a very
small number of rules for forming expressions, with no restrictions on
how they are composed, suffice to form a practical and efficient
programming language that is flexible enough to support most of the
major programming paradigms in use today.

编程语言不应该被设计成在功能之上叠加功能,但是通过消除弱点和限制条件而
创建额外的功能显得非常必要.Scheme对形成 ``表达式`` 论证了极少数的规则,至于
他们如何去组合，怎么能形成一个表达式是没有限制的，高效的编程语言是弹性
灵活的，能够支持今天使用的大多数主流的编程范式。


Scheme was one of the first programming languages to incorporate
first-class procedures as in the lambda calculus, thereby proving the
usefulness of static scope rules and block structure in a dynamically
typed language. Scheme was the first major dialect of Lisp to
distinguish procedures from lambda expressions and symbols, to use a
single lexical environment for all variables, and to evaluate the
operator position of a procedure call in the same way as an operand
position. By relying entirely on procedure calls to express iteration,
Scheme emphasized the fact that tail-recursive procedure calls are
essentially gotos that pass arguments. Scheme was the first widely
used programming language to embrace first-class escape procedures,
from which all previously known sequential control structures can be
synthesized. A subsequent version of Scheme introduced the concept of
exact and inexact number objects, an extension of Common Lisp's
generic arithmetic. More recently, Scheme became the first programming
language to support hygienic macros, which permit the syntax of a
block-structured language to be extended in a consistent and reliable
manner.

像lambda演算一样，通过 证明静态范围规则和在动态类型语言中的块结构的实
用性,Scheme是最早结合 ``first-class`` 过程编程语言之一。Scheme是lisp中第
一种从lambda表达式和Symbol中区分过程的方言，在Scheme中,想要全部变量使用
一个单一词汇的环境,并以与操作数相同的方式，评估过程调用的操作符位置。
全都赖以表示迭代的过程.Scheme强调了尾递归过程调用本质上是 传递参数的事
实。Scheme是第一个包含 ``first-class`` 转义程序的广泛使用的编程语言，由此
可以合成所有已知顺序的控制结构。 Scheme一系列版本都介绍了精确和不精确
的 number 对象的语法概念，Common Lisp 泛型算法的一种扩展. 近期，
Schemeb成为了首个支持 ``hygienic(卫生）`` micro的编程语言。 在某些情况下和
可靠方式下允许块结构语言的语法被继承.

1.2 Guiding principles(指导守则）
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To help guide the standardization effort, the editors have adopted a set of principles, presented below. Like the Scheme language defined in Revised5 Report on the Algorithmic Language Scheme [14], the language described in this report is intended to:

为了帮助直到标准工作，编者采用了下面的一系列准则.类似于已经在《算法语
言Scheme》上定义过的Scheme语言的R5报告，这个报告企图描述：

- allow programmers to read each other's code, and allow
  development of portable programs that can be executed in any
  conforming implementation of Scheme;


- 允许其他程序员阅读别人的代码，允许开发可以被执行在任何符合Scheme实
  现的便携式程序.

- derive its power from simplicity, a small number of generally useful core syntactic forms and procedures, and no unnecessary restrictions on how they are composed;

- Scheme的强大源于它的简单。通常有用的核心句法形式和过程占极少数，他
  们如何被组合没必要限制。

- allow programs to define new procedures and new hygienic syntactic forms;

- 允许程序定义新的过程和新的卫生句法形式。

- support the representation of program source code as data;

- 支持程序源代码作为数据的表示。

- make procedure calls powerful enough to express any form of
  sequential control, and allow programs to perform non-local control
  operations without the use of global program transformations;

- 让过程调用更加强大,足够表示任何顺序控制的形式，并且允许程序不使用全
  局程序变换,执行非本地控制的运算对象，

- allow interesting, purely functional programs to run indefinitely
  without terminating or running out of memory on finite-memory
  machines;

- 允许有趣，在内存有限的机器上无限运行而不终止或耗尽内存的纯功能性程
  序。

- allow educators to use the language to teach programming effectively, at various levels and with a variety of pedagogical approaches; and

- 允许教育工作者使用这门语言,用各种级别和各种类别的
  教学方法有效的教授编程，

- allow researchers to use the language to explore the design,
  implementation, and semantics of programming languages.

- 允许研究者使用这门语言来探索设计、实现编程语言的语义。

In addition, this report is intended to:

- allow programmers to create and distribute substantial programs

and libraries, e.g., implementations of Scheme Requests for
Implementation, that run without modification in a variety of
Scheme implementations;

- 允许编程者来创建和描述实质性程序和库。比如：为实现Scheme请求的实现,
  以及在大量Scheme实现过程中没有修复的东西。

- support procedural, syntactic, and data abstraction more fully by

allowing programs to define hygiene-bending and hygiene-breaking
syntactic abstractions and new unique datatypes along with
procedures and hygienic macros in any scope;

- 通过允许程序定义卫生-变形和卫生-破坏抽象语法，和在任何范围内的过程
  和卫生宏之间新的独特数据类型,更加全面的支持程序化，语义化，和数据抽
  象.

- allow programmers to rely on a level of automatic run-time type

and bounds checking sufficient to ensure type safety; and

- 允许程序员依赖程度的自动运行时类型和绑定检查足以确保类型安全.

- allow implementations to generate efficient code, without requiring
  programmers to use implementation-specific operators or
  declarations.

- 允许实现生成有效代码,无需程序员使特殊的运算对象或者声明来实现.

While it was possible to write portable programs in Scheme as
described in Revised5 Report on the Algorithmic Language Scheme, and
indeed portable Scheme programs were written prior to this report,
many Scheme programs were not, primarily because of the lack of
substantial standardized libraries and the proliferation of
implementation-specific language additions.

虽然可以编写可移植程序方案Revised5报告中所描述的算法语言计划,实际上便
携式计划程序写这份报告之前,许多计划项目没有,主要是因为缺乏实质性标准库
和特定于实现的语言增加的扩散。


In general, Scheme should include building blocks that allow a wide
variety of libraries to be written, include commonly used user-level
features to enhance portability and readability of library and
application code, and exclude features that are less commonly used and
easily implemented in separate libraries.

通常,Scheme应该包括允许各种被复写的库构建块,包括经常使用的用户级别的功
能功能-用来增强便携性和库的可阅读性,以应用代码,以及在剥离库中极少使用
的,容易实现的排除在外的功能.

The language described in this report is intended to also be backward
compatible with programs written in Scheme as described in Revised5
Report on the Algorithmic Language Scheme to the extent possible
without compromising the above principles and future viability of the
language. With respect to future viability, the editors have operated
under the assumption that many more Scheme programs will be written in
the future than exist in the present, so the future programs are those
with which we should be most concerned.


这个报告所描述的语言,试图向后兼容于R5报告中的Scheme程序,为了继承可能不
会违背上面的规则和程序的可行性功能.在尊重未来的可能性下,编者将会执行下
面的设想,很多目前存在的核心Scheme程序将会在未来重写,所以未来的程序才是
我们更加应该关注的东西.


The Translation End in <2019-12-30 Mon 02:04>
