==============================================
Scheme\ :sub:`Note`\ \ :sub:`1`\\_&\ :sub:`2`\
==============================================

    :Author: lyps



1 Scheme-Note-1-&-2
-------------------

1.1 DONE ChezScheme `Introduction <https://www.scheme.com/tspl4/intro.html#./intro:h0>`_ Added: ------ [2019-12-31 Tue 16:28]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1.1.1 Chapter 1.Introduction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Scheme是一种一般用途的计算机编程语言.它是高级语言,支持操作的数据结构
有: String,Lists,Vectors,也支持操作更加传统的数据,如: numbers 和
characters.然而Scheme经常用符号应用标识.丰富的数据类型集和灵活延展的控
制结构让他成为一种真正全能语言.

Scheme可以应用到写编辑器,优化编译,操作系统,图形包,专业系统,数值应用,财
务分析包,虚拟现实系统,以及其他你能想到的应用.Scheme是非常容易学习的语
言,因为它是基于少数的句法格式和语义概念,因为大多数实现的交互性都鼓励实
验.Schemes是一门想要全弄懂,有挑战性的语言.然而,想开发使用Scheme的全部潜
力的能力需要认真的学习和练习.

在不同机器上的用相同版本Scheme实现的Scheme程序是可高度便携移植的.
因为从程序员角度来说,机器的依赖关系几乎是完全隐藏的.它们的便携移植也是
不同实现的，因为Scheme 设计者们一群人的努力工作发布的一系列报告：
Scheme的 ``rxrs`` 系列报告。最近的 ``r6rs`` 报告，通过一组标准库集和定义新的可
移植库和顶级程序的的标准机制的可移植性。

尽管一些早期的Scheme系统是没效率和慢的，教新的基于编译器的实现速度很快。
程序的运行和与用低级语言写的等效程序相当。

有时仍然存在的相对效率低下，是由于运行时检查导致的，该运行时检查支持通
用算术和帮助程序员检测和纠正各种常见的编程错误。这些检查可能在众多实现
中被禁用。

Scheme 支持很多数据类型的值，或者对象，包括 strings , symbols
,lists,vectors,全部number 数据类型集，包括复杂，真实，任意精度范围的
number.

The storage required to hold the contents of an object is dynamically
allocated as necessary and retained until no longer needed, then
automatically deallocated, typically by a garbage collector that
periodically recovers the storage used by inaccessible objects. Simple
atomic values, such as small integers, characters, booleans, and the
empty list, are typically represented as immediate values and thus
incur no allocation or deallocation overhead. 

::

    保存对象内容所需要的存储是根据需要来动态分配的,并保留到不再需要为止。
    然后自动释放，通常由垃圾回收定期恢复不可访问的对象的存储，简单的Atomic值,如：
    small integer,characters,booleans,以及空 list，通常作为瞬时值来表示，
    因此不会带来分配或者重新分配的开销

Regardless of representation, all objects are first-class data values;
because they are retained indefinitely, they may be passed freely as
arguments to procedures, returned as values from procedures, and
combined to form new objects. This is in contrast with many other
languages where composite data values such as arrays are either
statically allocated and never deallocated, allocated on entry to a
block of code and unconditionally deallocated on exit from the block,
or explicitly allocated and deallocated by the programmer. 

::

    不管怎么表示，所有对象是首类 数据类型。因为它们是无限期保留的。它们可以作为过程的参数自由传递，
    从过程作为值返回，组合形成新对象。和很多其他语言相反，复合数据值，比如：array,是静态分配的,从不释放.
    在进入代码块时分配,在退出时无条件分配.

1.1.2 Chapter 2.Getting Started
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.1.2.1 Interacting with Scheme
:::::::::::::::::::::::::::::::

- 保持学习兴致很重要,我们喜欢和人交谈,是因为有一个交互的环境.能清楚的
  感知自己的相对对错.所以为了所谓的有用没有,我们需要一个交互环境来检测.应
  付我们自己的交互环境是黑板:你究竟学习了多少东西.欺骗你的眼睛的是
  Scheme在Emacs配置下的交互环境.加起来,就能达到欺骗自己的效果.

- 学习东西量大,不止一遍的有聪明人说,索引很重要.想想吧,我们使用了究竟多
  少年的搜索引擎.没有一份牛逼的索引,你怎么好意思对自己说是一个网民.

- 现在我们尝试使用索引的方式来学习东西

1.1.2.1.1 reader results
''''''''''''''''''''''''

main point
    - string constant

    - number

    - 分数

    - 小数

    - procedure

Interaction
    - 尝试自己动手试一下,自己去猜结果,eg:

      .. code:: scheme

          (cons (car '(a b c))
                (cdr '(e f g)))

Procedure
    - 一个过程的定义和使用

      .. code:: scheme

          ;; define a procedure
          (define square
            (lambda(n)
              (* n n)))
          ;; use the procedure
          (square 2)
          (square -200)
          (square 1/2)
          (square 0.5 #;(just test the comment))

      其中包括:

      ::

          - define : establishes variable bind
          - lambda : creates procedure
          - * : the multiplition proceure

      - 一个文件内的文件的调用

      .. code:: scheme

          (load "reciprocal.ss")
          (reciprocal 0)

.. code:: scheme

    (load "reciprocal.ss")
    (reciprocal (reciprocal (/ 1 10)))

1.1.2.1.2 Simple Expressions
''''''''''''''''''''''''''''

- constant data object

  - strings

  - numbers

  - symbols

  - lists

- Other Object types

.. code:: scheme

    2.2+1.1i

scheme numbers include:

- exact integer --- arbitrary size

- inexact integer --- floating point

- rational(有理数) --- arbitrary size

- real

- complex numbers


Any procedure applications,is written as ``(procedure arg ...)``.

无论何种运算,都有一种符号被使用.不管运算对象的优先级和关联性如何没有
复杂的规则.

要想计算更加复杂的公式,你需要在上面嵌套更多算数过程.

.. code:: scheme

    (/ (* 6/7 7/2)
       (- 4.5 1.5))

- aggregate data structures(集合数据结构).

  1. in many language,the basic aggregate data structure is array,but
     in the scheme,it is the ``list``.
     eg:

     .. code:: scheme

         (quote(1 2 3 4 5))

  2. use ``quote`` forces the list to be tarted as data.

     .. code:: scheme

         '(/ 2 3)                                ;in here,the results is error.should be (/ 2 3)

     .. code:: scheme

         (quote hello)

     - hello now is a symbol,because the quote expression not procedure
       application.

     - the symbol is prevent scheme system from treating hello as
       variable.

     - quote a list tell scheme system to treated the parenthesized
       form as a list.

     - quote a identifier tell scheme system to treated the identifiers
       as a symbol.

     - The shared notation allowed Scheme program to be represented as
       scheme data.

  3. car && cdr

     - Each require a non-empty list as its argument.

       ::

           > (cdr '())
           Exception in cdr: () is not a pair
           Type (debug) to enter the debugger.
           > (car '())
           Exception in car: () is not a pair
           Type (debug) to enter the debugger.

  4. cons

     - build a list

       - add a element to the beginning of the list,consing the element
         onto the list.

       - cons build pair,cdr of the pair could not a list.

     - conclusion

       - a list is a sequence of pairs,each pair's cdr is next pair in
         the sequence.

       - The cdr of the last pair in a proper list is empty list.

       - formally,empty list is a proper list,and pair whose cdr is a
         proper list is a proper list.

       - An improper list is printed in ``dotted-pairs``. 

         ::

             Chez Scheme Version 9.5
             Copyright 1984-2017 Cisco Systems, Inc.

             >(cons 'a 'b)
             (a . b)
             > (cdr '(a . b))

             > (cons '(a b) '(c d))
             ((a b) c d)
             > (list '(a b) '(c d))
             ((a b) (c d))
             > (cons 'a '(b . c))
             (a b . c)
             > (list 'a '(b . c))
             (a (b . c))
             > (cons 'a)

             Exception: incorrect argument count in call (cons (quote a))
             Type (debug) to enter the debugger.
             > (list 'a)
             (a)
             > (cons)

             Exception: incorrect argument count in call (cons)
             Type (debug) to enter the debugger.
             > (list)
             ()

1.1.2.1.3 Exercise
''''''''''''''''''

::

    > (car (list + - * /))
    #<procedure +>
    > ((car (list + - * /)) 2 3)
    5
    > (list 1 2)
    (1 2)
    > >
    #<procedure >>
    > (cdr '(2 3))
    (3)
    >(car (cdr '(2 3)))
    3
    > (car (car '(a b) '(c d)))
    Exception: incorrect argument count in call (car (quote (a b)) (quote (c d)))
    Type (debug) to enter the debugger.
    > (car (car '((a b)(c d))))
    a
    > (cdr (car '((a b)(c d))))
    (b)
    > (car (b))

    Exception: variable b is not bound
    Type (debug) to enter the debugger.
    > (= b (b))

    Exception: variable b is not bound
    Type (debug) to enter the debugger.
    > (cdr (cdr '((a b)(c d))))
    ()
    > (cdr (cdr '((a b)(c d))))
    () >(cdr '((a b)(c d)))
    ((c d))
    > (cdr '((a b)(c d)))
    ((c d))
    > (cdr '((c d)))
    ()
    > (car '((c d)))
    (c d)
    > (car (car '((c d))))
    c
    > (car (cdr '((a b)(c d))))
    (c d)
    > (car (car '((a b)(c d))))
    a
    > (cdr (car '((a b)(c d))))
    (b)
    > (car(cdr (car '((a b)(c d)))))

::

    > (cdr '((a b) ((c) d) () ()))
    (((c) d) () ())
    > (car '(((c) d) () ())
    )
    ((c) d)
    > (car '((c) d))
    (c)
    > (cdr '((c) d))
    (d)
    >(car '(d))
    d
    > (cons 'c 'd)
    (c . d)
    > (cons '(c . d) '(() . ()))
    ((c . d) ())
    > (cdr ((c . d) ()))

    Exception: invalid syntax (c . d)
    Type (debug) to enter the debugger.
    > (cdr '((c . d) ()))
    (())
    > (cons 4 5)
    (4 . 5)
    > (cons 4 5)
    (4 . 5)
    > (cons '(()) (cons 4 5))
    ((()) 4 . 5)
    > (cons '(2 . ((3) . ())) '((()) 4 . 5))
    ((2 (3)) (()) 4 . 5)
    > (cons ((3) . ()))

    Exception: attempt to apply non-procedure 3
    Type (debug) to enter the debugger.
    > (car '((3) . ()))
    (3)
    > (cdr '((3) . ()))
    ()
    > (cons 1 '((2 (3)) (()) 4 . 5))
    (1 (2 (3)) (()) 4 . 5)

.. code:: scheme

    (cons 'c '())
    (cons '(c) '(d))
    (cons '() '())
    (cons '((c) d) '(()))
    (cons '(a b) '(((c) d) ()))

1.1.2.2 Evaluating Scheme Expression
::::::::::::::::::::::::::::::::::::

the rules for constant objects such as strings or numbers: the object
itself is the value.

- Evaluating procedure of the form(procedure arg1 ... argn).

  - Find the value of procedure.

  - Find the value of arg1.

  - ...

  - Find the value of argn.

  - Apply the value of procedure to the values of arg1 ... rang.

- procedure application

  - quote expression

  - subexpression of a procedure application are evaluated

  - subexpression of a quote expression is not evaluated

- quote expression

  - evaluation of constant object

  - The value of a quote expression of the form is simply object.

- Constant Objects,procedure applications,quote expressions

  - core syntactic forms

  - syntactic extensions defined

- Syntactic forms & procedures & evaluation of procedure application

  - A scheme evaluator is free to evaluate the expressions in any
    order --- left to right,right to left,or any other sequential
    order.

  - procedure is often a variable that names a particular procedure.

    .. code:: scheme

        ((car (list + - * /)) 2 3)

    now the procedure is ``((car (list + - * /)) 2 3)``,the value of
    ``((car (list + - * /)) 2 3)`` is the additional procedure,just as if
    procedure were simply the variable ``+``.

1.1.2.3 Variables and let Expressions
:::::::::::::::::::::::::::::::::::::

supposed expr is a scheme expression that contains a variable ``var``.
Additionally,that we would like ``var`` to have the value **val** when we
evaluate expr.For example,we might like ``x`` to have the value 2 when we
evaluate ``(+ x 3)``.

.. code:: scheme

    (let ((x 2))
      (+ x 3))

so the let syntactic form includes a list of variable-expression
pairs,along with a sequence of expressions referred to as the body of
the let.

The inner binding for x is said to shadow the outer binding.A ``let-``
bound variable is visible everywhere within the body of its ``let``
expression except where it is shadowed.

The region where a variable binding is visible is called its ``scope``.

.. code:: scheme

    (let ([x 1])
      (let ([x (+ x 1)])
        (+ x x)))

- conclusion

  - The scope of the first x is the body of the outer let expression
    minus the body of the inner let expression,where it is shadowed by
    the second x.

  - This form of scoping is refereed to as lexical scoping.


- Exercise 

  - a

    .. code:: scheme

        (let ([a 1] [b 2])
          (let ([x (* 3 a)] [y b])
            (+ (- x y)
               (+ x y))))

.. code:: scheme

    (let([x (list 1 2 3)])
      (cons (car x)
            (cdr x)))

.. code:: scheme

    (let ([x 'a] [y 'b])
      (list (let ([new_x 'c]) (cons new_x y))
            (let ([new_y 'd]) (cons x new_y))))

.. code:: scheme

    (let ([x '((a b) c)])
      (cons (let ([x (cdr x)]) (car x))
            (let ([x (car x)])
              (cons (let ([x (cdr x)]) x)
                    (cdr x))))

.. code:: scheme

    (let ([x '((a b) c)][y (cdr x)][z (car x)])
      (cons (let ([x y])
              z)
            (let ([x z])
              (cons (let ([x y])
                      z)
                    (cons (let ([x z]))
                          x)
                    y))))

1.1.2.4 Lambda Expressions
::::::::::::::::::::::::::

.. code:: scheme

    (let ([x (* 3 4)])(+ x x))

- the variable ``x`` bound to the value of ``(* 3 4)``.

- now we want to the ``x`` bound to the value of ``(/ 99 11)``.or let ``x`` bound
  to the value of ``(- 2 7)``.

now we need a different ``let`` expression.so when the body of the ``let`` is
complicated,however,having to repeat it can be inconvenient.

so,we can use the syntactic form ``lambda`` to create a new procedure that
has ``x`` as a parameter and has the same body as the ``let`` expression.

.. code:: scheme

    (lambda (x) (+ x x))

so the lambda expression is ``(lambda (var ...) body1 body2 ...)``.

The most common operation to perform on a procedure is to apply it to
one or more values.

.. code:: scheme

    ((lambda (x) (+ x x))
     (* 3 4))

- The procedure is the value of ``(lambda (x) (+ x x))``.

- The only variable is ``(* 3 4)``.

- The argument values,or actual parameters,are bound to the formal
  parameters within the body of the ``lambda`` expression in the same way
  as ``let-`` bound variables are bound to their values.

- In this case, ``x`` bound to 12.


**Because procedures are objects,we can establish a procedure as the value of a variable and use the procedure more than one**.

.. code:: scheme

    (let ([double (lambda (x) (+ x x))])
      (list (double (* 3 4))
            (double (/ 3 3))
            (double (- 3 3))))

Here,the "double" binding to the procedure "lambda expression".Use
this procedure to double three different values.

- they may be collapsed into a single procedure by adding an
  additional procedures.

  .. code:: scheme

      (let ([double-any (lambda(f x)(f x x))])
        (list (double-any + 13)
              (double-any cons 'a)))

.. code:: scheme

    (let ([x 'a])
      (let ([f (lambda(y)(list x y))])      ; The variable "x" is occur free or free variable.
        (f 'b)))

- the variable x is said to occur free in the lambda expression.

- the variable y does not occur free in the lambda expression since it
  is bound by the lambda expression.


the procedure is applied somewhere outside the scope of the bindings
for variables that occur free within the procedure.

.. code:: scheme

    (let ([f (let ([x 'sam])
               (lambda (y z)(list x y z)))])
      (f 'i 'am))

- same binding that were in effect

- the procedure created are in effect again

- the procedure is applied

.. code:: scheme

    (let ([f (let ([x 'sam]) (lambda (y z)(list x y z)))])
      (let ([x 'not-sam])
        (f 'i 'am)))

you could find the procedure named of ``f``,the value of x is ``sam``.

a let expression is nothing more than the direct application of a
lambda expression to a set of argument expression.

for example,the two expression below are equivalent.

.. code:: scheme

    (let ([x 'sam])
      (cons x x))

.. code:: scheme

    ((lambda(x)(cons x x)) 'sam)


- conclusion

  - let expression is a syntactic extension defined in terms of lambda
    and procedure application.

  - the first expression:

    ::

        (let ((var expr) ...) body1 body2 ...)

  - the second expression:

    ::

        ((lambda (var ...) body1 body2 ...)
        expr ...)


the formal parameter specification,(var ...),need not be a proper
list,or indeed even a list at all.

The formal parameter specification can be any of the following three
forms:

- a proper list of variable,(var1 ... varn),which as we have already seen.

- a single variable, ``varr`` ,or

- an improper list of variables, (var1 ... varn . varr).


- exactly n parameters must be supplied,and each variable is bound to
  the corresponding actual parameter.

  .. code:: scheme

      (let ([f (lambda x x)])
        (f 1 2 3 4))

  .. code:: scheme

      (let ([f (lambda x x)])
        (f))

  ``f`` accepts any number of arguments.These arguments are automatically
  formed into a list to which the variable x is bound;the value of f
  is this list.so,the first arguments are 1,2,3,and 4,so the answer is
  (1 2 3 4).

  ::

      > ((lambda x x))
      ()
      > ((lambda (x) x) 1)
      1

- any number of actual parameter is valid,all of the actual parameters
  are put into a single list and single variable is bound to this
  list.

  .. code:: scheme

      (let ([g (lambda (x . y) (list x y))])
        (g 1 2 3 4))

  The value of the procedure named g is a list whose first element is
  the first argument and whose second element is a list containing the
  remaining arguments.

  while ``f`` accepts any number of arguments, ``g`` must receive at least
  one.

- hybrid of the first two cases.

  .. code:: scheme

      (let ([h (lambda (x y . z)(list x y z))])
        (h 1 2 3 4))

- EXERCISE

  .. code:: scheme

      (let ([f (lambda (x) x)])
       (f 'a))                                ; 'a
      (let ([f (lambda x x)])
       (f 'a))                                ; (list 'a)
      (let ([f (lambda (x . y) y)])
       (f 'a))                                ; ()

1.1.2.5 Top-Level Definitions
:::::::::::::::::::::::::::::

.. code:: scheme

    (define double-any
      (lambda (f x)
        (f x x)))

.. code:: scheme

    > caar
    #<procedure caar>
    > (car '(a b c))
    a
    > (caar '((a b)a b c))
    a
    > (cddr '(a b c))
    (c)
    > (cddr '(a))

    Exception in cddr: incorrect list structure (a)
    Type (debug) to enter the debugger.
    > (cddr '(a b))
    ()
    > (caaar '(((a b) c) d))
    a
    > 

- doubler --- one argument

  - f --- two argument

  - returned by doubler accepts one argument

  - define with doubler 

    - simple double

    - double-cons

    .. code:: scheme

        (define doubler
          (lambda (f)
            (lambda (x)(f x x))))

        (define double (doubler +))
        (double 13/2)                           ;13

        (define double-cons (doubler cons))
        (double-cons 'a)

define a ``double-any``.

.. code:: scheme

    (define doubler
      (lambda (f)
        (lambda (x) (f x x))))

    (define double-any
      (lambda(f x)
        ((doubler f) x)))                    ;double & double-cons,f has the appropriate value.

    (double-any + 3)
    (double-any double-any double-any)

- if we attempt to use a variable that is not bound by a let or lambda
  expression and that does not have a top-level definition?

.. code:: scheme

    ;; (i-am-not-defined 3)
    (define proc2 cons)
    (define proc1
      (lambda(x y)
        (proc2 y x)))

    (proc1 'a 'b)

1.1.2.6 Conditional Expressions
:::::::::::::::::::::::::::::::

.. code:: scheme

    (define abs
      (lambda(n)
        (if (< n 0)
            (- 0 n)
            n)))
    (abs -1)

- if expression

  - form (if test consequent alternative)

    - consequent is the expression to evaluate if test is true and
      alternative is the expression to evaluate of test is false

    - In the expression above,test is (< n 0),consequent is ``(-0 n)``,and
      alternative is n.

- more written methods:

  .. code:: scheme

      (define abs
        (lambda (n)
          (if (>= n 0)
              n
              (- 0 n))))

      (define abs
        (lambda(n)
          (if (not (< n 0))
              n
              (- 0 n))))

      (define abs
        (lambda(n)
          (if (or (> n 0)(= n 0))
              n
              (- 0 n))))
      (define abs
        (lambda (n)
          (if (= n 0)
              0
              (if (< n 0)
                  (- 0 n)
                  n))))

  .. code:: scheme

      (define abs
        (lambda(n)
          ((if (>= n 0) + -)
           0
           n)))
      (abs -99)

``if`` is a syntactic form and not a procedure.

.. code:: scheme

    (pair? '(1 2 3))
    (pair? '(1 . 2))

.. code:: scheme

    (eqv? (cons 'a 'b) (cons 'a 'b))
