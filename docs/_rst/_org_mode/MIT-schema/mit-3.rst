========
高阶过程
========

    :Author: lyps



1 高阶过程
----------

1.1 TODO `{SICP}计算机程序的构造和解释（Lec2a：高阶过程） 中英双语字幕 - YouTube <https://www.youtube.com/watch?v=mrgcGvOI1bs&list=PLkEwH_Z2WOlppy8oUfrGwFVlOuKyo3RO_&index=3>`_ Added: ------ [2019-12-25 Wed 16:27]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[2019-12-25 Wed 16:27]  

1.1.1 sum-int
^^^^^^^^^^^^^

求 从 a 到 b的和

.. code:: scheme

    (define (sum-int a b)
      (if (> a b)
          0
          (+ a
    	 (sum-int (1+ a) b))))		;主要思想就是将其分解为子问题,总是比整个问题的规模少1的整数序列
    (sum-int 1 2)

1.1.2 square-int
^^^^^^^^^^^^^^^^

求a的平方 到 b的平方之和

.. code:: scheme

    (define (square x)
      (if (= x 0)
          0
          (* x x)
          ))

    (define (square-int a b)
      (if (> a b)
          0
          (+ (square a)
    	 (square-int (1+ a) b))))

    (square-int 2 4)

1.1.3 sum-one
^^^^^^^^^^^^^

可以观察到,这两个程序非常相似,问题不在于重复写同一件事情而带来的时间浪
费,而在于里面的一些非常简单的思想.

如果要设计一个极其复杂的系统,将问题拆分成尽量多的模块是很重要的.并且每
一个模块都要能够被独立的解释.


我们知道在不依赖具体内容的情况下,把东西加起来的方法.这样我们只需要做一
次调试,做一次分析.还能和其他用户分享这段程序. 

1.1.4 莱布尼兹公式的实例印证
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code:: scheme

    (define (pi-sum a b)
      (if (> a b)
          0
          (+ (/ 1.0 (* a (+ a 2)))
    	 (pi-sum (+ a 4) b))))		;因为这个公式的叠加起来的步长是 4.这样也就计算好了这个程序的下一个上界是 (+ a 4)
    (pi-sum 1 3)

1.1.5 sum-two
^^^^^^^^^^^^^

让我们观察一下这3个程序的共同点吧.所以就会有下面的式子:

general pattern(公共模式):

.. code:: scheme

    (define (<name> a b)
      (if (> a b)
          0
          (+ (term a)
    	 (<name> (<next> a) b))))

::

    <name> --- 函数名
    a --- 下界
    b --- 上界

    term --- 对下界的下标进行处理
    <next> --- 下一个下界

    将最终结果递归的叠加起来

你给各种数据命名看上去也是很合理的事情.比如说 ``procedure`` ,毕竟现在很多
语言都允许使用过程参数.

1.1.6 procedure arguments
^^^^^^^^^^^^^^^^^^^^^^^^^

在讨论过程参数之前,我们需要做一些别的事情:定义我们的求和记法.

.. code:: scheme

    (define (SUM TERM A NEXT B)
      ;; TERM & NEXT 不是数字参数,是对数字进行计算的过程参数.
      ;; TERM: 给TERM这个过程提供一个下标,TERM 会计算出这个下标所对应的项的值.
      ;; NEXT: NEXT过程会根据一个下标来计算下一个下标.用来计数使用.
      (if (> A B)
          0
          (+ (TERM A)
    	 (SUM TERM
    	      (NEXT A)
    	      A
    	      NEXT
    	      B))))

::

    可以看出,我们有四个参数:过程,下界的下标,获得下一个下标的方法,上界.
    你可以看到在递归调用的时候,传递的是同一个过程. 
    - 我们会再一次需要 TERM
    - 下一个下标是用NEXT过程计算出来的
    - 计算下一个下标的过程,也是单独的需要,因为这两者是不同的.
    因为计算下一个下标的过程与下一个下标是不同的.下一个下标是根据上一个下标求得的.

.. code:: scheme

    ;; sum 的原始实例
    (define (SUM-INT A B)
      (define (IDENTIFY X) X 		;因为Term 对数字计算结果还是原数字.所以这里的 IDENTIFY 就相当于 (lambda(x) x).
        (SUM IDENTIFY A 1+ B))) 		;使用A作为初始下标,1+过程来获取下一个下标,B作为上界.

.. code:: scheme

    (define (SUM-SQ A B)
      (SUM SQUARE A 1+ B))		;每个下标使用SQUARE这个过程来产生一个项,A作为下界, 1+ 过程产生下一个项的方法,B是上界,

.. code:: scheme

    ;; pi-sum 实例只需要将累加的每一项从累加过程中独立出来,
    ;; 其实观察之前的式子有一个表达式用来计算 TREM,所以我们需要找一个表达式的过程来计算对应下标的值.
    (define (PI-SUM A B)
      (SUM
       (lambda(i)(/ 1.0 (* i (+ i 2))))
       A
       (lambda(i)(+ i 4))
       B))
    ;; 这种将过程用作过程参数的方法,允许我们将很多过程组合成为一个过程.

1.1.7 第二部分
^^^^^^^^^^^^^^

我们学习计算机,是为了计算机帮我们达成某种目的.而并非成为它的奴隶.抽象
很显然是一种能够轻易操控程序的方法.

1.1.7.1 如何使用抽象来解释程序在干什么?
:::::::::::::::::::::::::::::::::::::::

比如说,我们现在在数学上有一个对平方根结果更好的猜测.

1.如果Y是平方根的一个猜测值,那么我们有一个函数F. 用来改进猜测值.我们使
用 (/ 2 (+ Y (/ X Y)) 作为改进平方根的猜测值.

2.F对根号X的求值结果就是根号X.如果你将Y替换为 根号x,你会得到 

square of x = (/ 2 (+ sqrt(x) (/ x sqrt(x))))

3.实际上我们就是在寻找函数F的一个不动点.

4.跟这个比较类似的例子就是,用计算器不断的演算cos(x)的值.所以我们可以通
过迭代的方法找到一些函数的不动点.


总结如下:

.. image:: ../../../_static/get_square_root.png


.. code:: scheme

    (define (SQRT X)
      (FIXED-POINT
       (lambda(Y)(AVERAGE(+ Y (/ X Y))))
       1)) 					;我们的初始猜测值 为 1

.. image:: ../../../_static/get_fixed_point.png


.. code:: scheme

    ;; 现在我们想具体构建 求取 不动点的过程
    (define (FIXED-POINT f start)
      "我们想要找到函数函数 f 的不动点,需要从一个具体的点开始"
      (define (Iter OLD NEW)
        "内部的循环定义为内部过程,最终其将收敛到一点."
        (if (close-enough? OLD NEW)
    	NEW
    	(Iter NEW (f new)))))

所以,这就直接映射了之前的程序

.. code:: scheme

    (define (fixed-point f start)
      ;; 定义精度
      (define tolerance 0.00001)
      ;; 检测两个值是否接近
      (define (close-enuf? u v)
        (< (abs (- u v)) tolerance)
        )
      ;; 内部迭代过程
      (define (iter OLD NEW)
        (if close-enuf?)
        NEW
        (iter new (f new)))
      ;; 入口
      (iter start (f start))
      )
