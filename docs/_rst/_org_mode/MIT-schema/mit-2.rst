===============
SCIP-2-计算过程
===============

    :Author: lyps



1 TODO `{SICP}计算机程序的构造和解释（Lec1b：计算过程） 中英双语字幕 - YouTube <https://www.youtube.com/watch?v=WuK9NmA3aq0&list=PLkEwH_Z2WOlppy8oUfrGwFVlOuKyo3RO_&index=2>`_ Added: ------ [2019-12-17 Tue 18:37]
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

[2019-12-17 Tue 18:37]  

1.1 Section 1
~~~~~~~~~~~~~

如果你想你写的代码高效,你必须理解你写下的代码,和你想要控制的程序的行为
之间的联系. 所以我们试图以尽可能清晰的方式建立这样的联系.  

我们尤其要理解,特定过程和表达式的模式,将会导致何种的特定的执行模式,和
计算过程的特定行为.

1.1.1 show case
^^^^^^^^^^^^^^^

计算两个数的平方根表达式

.. code:: scheme

    (define (SQ x)
      (+ (* x x)))
    (define (SOS x y)
      (+ (SQ x)(SQ y)))
    (SOS 1 2)

如果我们要理解程序的过程以及它的控制方法,必须将这个过程的机制,与过
程执行时所产生的行为对应起来. 

1.1.2 Substitution Mode
^^^^^^^^^^^^^^^^^^^^^^^

代换模型可以帮助我们理解过程和程序执行的原理. 以及过程如何使得程序执行.

1.1.3 Kinds of Expressions
^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    常规形式:
    numbers
    symbols
    特殊形式:
    lambda expressions
    definitions
    conditionals
    combinations

questions:

1.如何对组合式(combinations)求值.

1.1.4 Substitution Rules
^^^^^^^^^^^^^^^^^^^^^^^^

::

    To evaluate an application:
    1.Evaluate the operator to get procedure 
    2.Evaluate the operands to get arguments
    3.Apply the procedure to the arguments 
      (1.Copy the body of the procedure
      (2.Substituting the arguments supplied for the formal parameters of the procedure 
      (3.Evaluate the resulting new body

1.1.4.1 DONE Simple case
::::::::::::::::::::::::

.. code:: scheme

    ;; 基本代换模型
    (SOS 3 4)
    (+ (SQ 3) (SQ 4))
    (+ (* 3 3) (SQ 4))
    (+ 9 (* 4 4))
    (+ 9 16)

    25

1.1.5 think in complex things
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

理解复杂事情的时候,你需要忽略细节,关键在于避免不必要的观察,计算和思考.

1.1.6 IF Expression
^^^^^^^^^^^^^^^^^^^

::

    To evaluate an IF Expression
       Evaluate the predicate expression(条件表达式)
       if it yields TRUE
           evaluate the consequent expression(结果表达式)
       otherwise
           evaluate the alternative expression(选择表达式)

知道事物各部分的名称,或者表达式各部分的名称很重要.

魔法师告诉了你,如果你能叫出某个精灵的名字,你就会拥有控制它的能力.所以
你需要掌握这些名称.

1.1.7 DONE Simple Case
^^^^^^^^^^^^^^^^^^^^^^

.. code:: scheme

    (define (+ x y)
      (if (= x 0)
          y
          (+ (-1+ x)(1+ y))))


    (+ 3 4)

    (if (= 3 0)
        4
        (+ (-1+ 3) (1+ 4)))

    (+ (-1+ 3)(1+ 4))
    (+ (-1+ 3) 5)
    (+ 2 5)

    (if (= 2 0)
        5
        (+ (-1+ 2) (1+ 5)))

    (+ (-1+ 2)(1+ 5))
    (+ 1 (!+ 5))
    (+ 1 6)

    (if (= 1 0)
        6
        (+ (-1+ 1) (1+ 6)))

    (+ (-1+ 1) (1+ 6))
    (+ 0 (1+ 6))
    (+ 0 7)

    (if (= 0 0)
        7
        (+ (-1+ 0)(+1 7)))

    if yield TRUE

    The result is  7

``-1+ , 1+``  both are primitive operations.

1.2 Section 2
~~~~~~~~~~~~~

现在我们有一个统一的机制,去理解一个由过程和表达式组成的程序是如何演化
成计算过程的.

只有拥有程序如何演化成计算过程的直觉,程序到底是什么形状的直觉,这样才能产生
特定形状的计算过程.即达到一种预先预览的效果.

1.2.1 Complex Case
^^^^^^^^^^^^^^^^^^

.. code:: scheme

    "Two ways to add whole numbers,重要的是,理解程序执行的时候,计算过程中的值是怎样的"

    ;; one method
    (define (+ x y)
      (if (= x 0)
          y
          (+ (-1+ x)(1+ y))))

    ;; the second method
    (define (+ x y)
      (if (= x 0)
          y
          ((1+ (+ (-1+ x) y)))))

    (+ 3 5)
    (if (= 3 5)
        5
        (1+ (+ (-1+ 3) 5)))

    (1+ (+ (-1+ 3) 5))
    (1+ (+ 2 5))

    (1+ (+ 2 5))
    (1+ (if (= 2 0)
    	5
    	(1+ (+ (-1+ 2) 5))))
    (1+ (1+ (+ (-1+ 2) 5)))
    (1+ (1+ (+ 1 5)))

    (1+ (1+ (if(= 1 0)
    	   5
    	   (1+ (+ (-1+ 1) 5)))))

    (1+ (1+(1+ (+ (-1+ 1)5))))

    (1+(1+(1+ (+ 0 5))))
    (1+(1+(1+ 5)))
    (1+(1+ 6))
    (1+ 7)
    8

这两个程序的结果一样,但是过程并不一致,唯一的区别是把加1的过程放在哪.

1.2.2 IN-PROGRESS Compare to these Procedures
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

需要我们比较这两个程序的计算过程. `1.1.7 DONE Simple Case`_

The first procedure

.. code:: scheme

    (+ 3 4)
    (+ 2 5)
    (+ 1 6)
    (+ 0 7)

    "
    	  |
    	  |
    	  |
    	  |
    	  | time(随着x的增大而增大,与x成正比)
    ---------- space(横向空间一直不变)

    所以
    time(时间复杂度) = o(x)
    space(空间复杂度) = o(1)

    对于这样的计算过程,我们称之为 迭代过程(Interation)
    "

The second procedure

.. code:: scheme

    (+ 3 4)
    (1+ (+ 2 4))
    (1+ (1+ (+ 1 4)))
    (1+ (1+ (1+ (+ 0 4))))
    (1+ (1+ (1+ 4)))
    (1+ (1+ 5))
    (1+ 6)
    7
    "
    	    |
    	    |
    	    |
    	    |
    	    | time(随着x的增大而增大,与x成正比)
      ---------- space(随着x的增大而增大,正比于输入参数)

    time = o(x)
    space= o(x)

    对于这样的计算过程,我们称之为 线性递归过程(Linear Recursion)
    "

这两种过程都是递归定义的,也就是两个过程的定义中都引用了该过程本身.但是
他们却产生了不同形状的计算过程.

过程定义是递归的,产生的过程也是递归的.

另外一种解释,就是典型的官僚主义方式比较法

.. image:: /_static/Interation.png
    :alt: alternate text
    :align: center
    :scale: 50%
