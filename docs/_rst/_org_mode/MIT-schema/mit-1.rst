======
SCIP 1
======

    :Author: lyps

-**- mode:org; -**-

1 schema primitive
------------------

1.1 test
~~~~~~~~

.. code:: common-lisp
    :name: hello-world

    (princ message)

1.2 means of combination
~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: scheme

    (+ 3 2
       (* 3
          (+ 7 19.5)
          (+ 2 2))
       2)

1.3 means of abstraction
~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: scheme

    (define B (* 5 5))
    B					;25
    (* B B)					;625

    (define A (+ B
    	     (* 5 B)))
    A


.. code:: scheme

    (write "hello world")
    (+ 2 2)

1.3.1 define a square
^^^^^^^^^^^^^^^^^^^^^

.. code:: scheme

    (define (square x) (* x x))
    (square 1001)

.. figure:: /_static/mit-1-2.png
    :alt: alternate text
    :align: center
    :scale: 50%


    deine a procedure

.. code:: scheme

    ;; lambda to be used build a procedure
    (define SQUARE (lambda (x) (* x x)))
    (SQUARE 1002)
    (SQUARE (SQUARE (SQUARE 1000)))
    SQUARE

.. figure:: /_static/mit-1-3.png
    :alt: alternate text
    :align: center
    :scale: 50%


    lambda --> make a procedure

1.3.2 two example
^^^^^^^^^^^^^^^^^

.. code:: scheme

    ;; get the average of x and y
    (define (average x y)
      (/ (+ x y) 2))

.. code:: scheme

    ;; get a mean-average of x and y
    (define (mean-square x y)
      (average  (square x) (square y)))
    +

The key thing here is that,having defined ``square``, we could it as if it
were primitive.

So define ``mean-square`` people doesn't have to know,at this
point,whether ``square`` was something built into the language,or whether
it was a procedure that was defined.

That's key thing about lisp.That happen to be primitive in the
language and things that happen to be built in.

the key thing is you can't find the different between things that are
build in and things that are compound.

Because the things that are compound have an abstraction wrapper
wrapped around them.

1.4 how to make a case analysis
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

the mathematical definition of the absolute value functions.

::

              1. x   for x < 0
    abs(x)=   2. 0   for x = 0 
              3. -x  for x < 0

this means case analysis, ``COND`` conditional.

.. code:: scheme

    (define (Abs x)
      (cond ((< x 0) (- x))
    	((= x 0) 0)
    	((> x 0) x)
    	))
    (Abs -2)


(< x 0) called predicate or condition, the scheme no ``-x``, just have ``- x``.

1.5 another case analysis
~~~~~~~~~~~~~~~~~~~~~~~~~

often,you have a case analysis where you only have one case.where you
test something,and then depending on whether it's true or false,you do
something.

And here's another definition of absolute value.

.. code:: scheme

    (define (abs x)
      (if (< x 0) 				; one conditional
          (- x) x) ; two results
      )
    (abs 0)

``if`` and ``cond`` just like a syntactic sugar.

1.6 above summary
~~~~~~~~~~~~~~~~~

1. define a ``symbol`` >>  what you're define it to be.

1.7 advanced square methods
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: /_static/mit-1-4.png
    :alt: alternate text
    :align: center
    :scale: 50%


    find an approximation to the square root of X

1. make a guess.

2. improve the guess by Average(G,X/G).

3. good-enough of the guess.

4. Use 1 As an initial guess.

1.8 TODO try
~~~~~~~~~~~~

<2019-12-09 Mon 23:41>

now we want to get the value of square of root x.

1.define the procedure 

.. code:: scheme

    (define (sqrt-item guess x)
      (if (good-enough? guess x)
          guess
          ;; if not good-enough,continue to guess.
          (sqrt-item (improve guess x) x)))

    ;; how to get good enough ?
    (define (good-enough? guess x)
      (<
       ;; abs = absolute value
       ;; square = ping fang
       ;; | guess * guess - x| < 0.001
       (abs (- (square guess) x))
       0.001))

    (define (square x)
      (* x x)
      )
    ;; how to improve guess ?
    (define improve(guess x)
      (average guess (/ x guess))
      )

.. code:: scheme

    (define (average x y)
      (/ (+ x y) 2))
    (average 1.0 2)

.. code:: scheme

    (define (square x) (* x x))
    (square 2)

2.beginning for sqrt

.. code:: scheme

    (define sqrt
      (sqrt-iter 1.0 x))

3.test the procedure 

guess = 1 ,x = 2

::

    (1 2)
    good-enough ?
                false
                (1 2)
                improve
                (1 + 2/1) / 2 = 1.5
                (1.5 2)

    good-enough ?
                |1.5 * 1.5 - 2| = .25 > 0.001 
                false
                improve
                (1.5 + 2/1.5) / 2 = 1.416
                (1.416 2)

1.9 question
~~~~~~~~~~~~

.. code:: scheme

    (define a (* 5 5))
    a					;25
    (a)					;error

.. code:: scheme

    (define (b) (* 5 5))
    b					; procedure 
    (b)					;25
