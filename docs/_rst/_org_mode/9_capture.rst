================
Org Mode Capture
================

    :Author: lyps
    :Contact: lyps@github.com
    :Date: <2019-11-30 Sat>

-**- mode:org; -**-

1 Capture,Refile,Archive
------------------------

An important part of any organization system is the ability to quickly
capture new ideas and tasks,and to associate reference material with
them. Org does this using a process called ``capture``.It also can store
files related to a task(attachments) in a special directory.Once in
the system,tasks and projects need to be moved around. Moving
completed project trees to an archive file keeps the system compact
and fast.

1.1 Capture
~~~~~~~~~~~

Capture lets you quickly store notes with little interruption of your
work flow.

1.1.1 Setting up capture
^^^^^^^^^^^^^^^^^^^^^^^^

The following customization sets a default target file for notes.

.. code:: common-lisp

    (setq org-default-notes-file (concat org-directory "/notes.org"))

You may also define a global key for capturing new material.

1.1.2 Using capture
^^^^^^^^^^^^^^^^^^^

``org-capture``
       Display the capture templates menu.If you have templates
       defined,it offers templates for selection or use a new Org
       outline node as the default template.It inserts the template
       into the target file and switch to an indirect buffer narrower
       to this new node.You may then insert the information you want.
       
``org-capture-finalize``
         Once you have finished entering information into the capture
    buffer, ``C-c C-c`` returns you to the windows configuration before
    the capture process,so that you can resume your work without
    further distraction.When called with a prefix argument,finalize
    and then jump to the captured item.
