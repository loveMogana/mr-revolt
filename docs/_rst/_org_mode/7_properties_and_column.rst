======================
Properties and Columns
======================

    :Author: lyps

-**- mode:org; -**-

1 Properties and Columns
------------------------

A property is a key-value pair associated with and entry.Properties
can be set so they are associated with a single entry,with every
entry in a tree,or with every entry in an Org file.

There are two main applications for properties in Org mode.First
property  are like tags,but with a value,Image maintaining a file
where you document bugs and plan releases for a piece of software
. Instead of using tags like ``release_1`` , ``release_2``, you can use a
property, say ``Release``, that in different subtrees has different
values,such as ``1.0`` ,that in different subtrees has different
values,such as ``1.0`` or ``2.0``. Second, you can use properties to
implement(very basic) database capabilities  [1]_  in an Org
buffer.Imagine keeping track of your music CDs,where properties
could be things such as the album,artist,date of release , members
of tracks ,and so on.

Properties can be conveniently edited and viewed in column view (see
Section `1.5 Column View`_).

1.1 Property Syntax
~~~~~~~~~~~~~~~~~~~

Properties are key-value pairs.when they are associated with a
single entry or with  a tree they need to inserted to a special
drawer with the name ``PROPERTIES`` ,which has to be located right
below a headline,and its planning line when applicable.Each
property is specified on a single line,with the key - surrounded by
colon -- first ,and the value after it.

key are case-insensitive.Here is an example:

`Case Insensitive property <~/org/revolt/docs/_rst/_org_demo/_customize_property>`_

Depending on the value of ``org-use-property-inheritance``, a property
set this way is associated either with a single entry, or with the
sub-tree defined by the entry.

You may define the allowed values for a particular property ``'Xyz'`` by
setting a property ``'Xyz_ALL'`` This special property is inherited ,so
if you set it in a level 1 entry,it applies to the entire tree.

when allowed values are defined,setting the corresponding property
becomes easier and is less prone to typing errors.For the example with
the CD collection ,  we can pre-define publishers and the number of
disks in a box like this:

`pre-defined properties <~/org/revolt/docs/_rst/_org_demo/_customize_property>`_

if you want to set properties that can be inherited by any entry in
a file,use a line like:

::

    .#+PROPERTY: NDisks_ALL 1 2 3 4

if you want to add to the value of an existing property , append a ``+``
to the property name.The following results in the property ``'var'``
having the value ``'foo=1 bar =2 '``.

::

    #+PROPERTY: var foo = 1
    #+PROPERTY: var+ bar = 2

It is also possible to add to the values of inherited properties.The
following results in the ``'Genres'`` property having the  ``'Classic Baroque'`` under the ``'Goldberg Variations'`` subtree.

`one property has more values <~/org/revolt/docs/_rst/_org_demo/_customize_property>`_

Note that a property can only have one entry per drawer.

Property values set with the global variable ``org-global-properties``
can be inherited by all entries in all org files.

1.2 Special properties
~~~~~~~~~~~~~~~~~~~~~~

Special properties provide an alternative access method to Org mode
features,like the TODO state or the priority of an entry,discussed in
the previous chapters.This interface exists so that you can include
these states in a column view, or to use them in queries.The following
property names are special and should not be used as keys in the
properties drawer:

.. table::

    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | properties            | implication                                                                                                                           |
    +=======================+=======================================================================================================================================+
    | ALLTAGS               | All tags ,including inherit ones                                                                                                      |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | BLOCKED               | t if task is currently blocked by children or siblings                                                                                |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | CATEGORY              | the category of an entry                                                                                                              |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | CLOCKSUM              | the sum of clock intervals in the subtree. org-clock-sum                                                                              |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | CLOCKSUM\ :sub:`T`\   | The sum of CLOCK intervals in the subtree for today.org-clock-sum-today must be run first to compute the values in the current buffer |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | CLOSED                | when was this entry closed ?                                                                                                          |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | DEADLINE              | the deadline time string,without the angular brackets                                                                                 |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | FILE                  | the filename the entry is located in                                                                                                  |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | ITEM                  | The headline of the entry                                                                                                             |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | PRIORITY              | the priority of the entry,a string with a single letter                                                                               |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | SCHEDULED             | the scheduling timestamp , without the angular brackets                                                                               |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | TAGS                  | The tags defined directly in the headline                                                                                             |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | TIMESTAMP             | the  first keyword-less timestamp in the entry                                                                                        |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | TIMESTAMP\ :sub:`IA`\ | The first inactive timestamp in the entry                                                                                             |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+
    | TODO                  | The todo keywords of the entry                                                                                                        |
    +-----------------------+---------------------------------------------------------------------------------------------------------------------------------------+

1.3 Property Searches
~~~~~~~~~~~~~~~~~~~~~

- ``org-match-sparse-tree``

- ``org-agenda m`` & ``org-tag-view``

- ``org-agenda M``

1.4 Property Inheritance
~~~~~~~~~~~~~~~~~~~~~~~~

The outline structure of Org documents lends itself to an
inheritance model of properties: if the parent in a tree has a
certain property,the children can inherit this property.Org mode
does not turn this on by default,because it can slow down property
searches significantly and is often not needed.

However,if you find inheritance useful,you can turn it on by
setting the variable ``org-use-property-inheritance``.It may be set to
``t`` to make all properties inherited from the parents, to a list of
properties that should be inherited,or to a regular expression that
matches inherited properties.If a properties has the value ``nil``,this
is interpreted as an explicit un-define of the property,so that
inheritance search stops at this value and return ``nil``.

Org mode has a few properties fort which inheritance is hard-coded,
at least for the special applications for which they are used:

``COULMNS``
                   The ``'COLUMNS'`` property defines the format of column
    view [see `1.5 Column View`_]. It is inherited in the sense
    that the level where a ``'COLUMNS'`` property is defined
    is used as the starting point for a column view
    table, independently of the location in the subtree
    form where columns view is turned on.

``CATEGORY``
                    For agenda view,a category set through a ``'CATEGORY'``
    property applies to the entries subtree.

``ARCHIVE``
                   For arching , the ``ARCHIVE`` property applies to the
    entries subtree.

``LOGGING``
                   The ``'LOGGING'`` property may define logging settings
    for an entry or a subtree.

1.5 Column View
~~~~~~~~~~~~~~~

A great way to view and edit properties in an outline tree is
``column view``. In column view,each outline node is turned into a
table row.Columns in this table provide access to properties of the
entries. Org mode implements columns by overlaying a tabular
structure over the headline of each item.While the headlines have
been turned into a table row,you can still change the visibility of
the outline tree.For example,you get a compact table by switching
to "content" view - S-TAB S-TAB, or simply c while column view is
active -- but you can still open,read,and edit the entry below each
headline.Or, you can switch to column view after executing a sparse
tree command and in this way get a table only for the selected
items. Column view also works in agenda buffers,where queries have
collected selected items,possibly from a number of files.

1.5.1 Defining columns
^^^^^^^^^^^^^^^^^^^^^^

Setting up a column view first requires defining the columns,This
is done by defining a column format line.

1.5.2 Scope of column definitions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To define a column format for an entire file,use a line like:

::

    #+COLUMN: %25ITEM %TAGS %PRIORITY %TODO

To specify a format that only applies to a specific tree,add a
``'column'`` property to the top node of that tree,for example:

`column view <~/org/revolt/docs/_rst/_org_demo/_customize_property>`_

If a ``'COLUMN'`` property is present in an entry,it defines columns
for the entry itself, and for the entire subtree below it. Since
the column definition is part of the hierarchical structure of
the document,you can define columns on level 1 that are general
enough for all sublevel,and more specific columns further
down,when you edit a deeper part of the tree.

1.5.3 column attributes
^^^^^^^^^^^^^^^^^^^^^^^

A column definition sets the attributes of a column. The general
definition looks like this:

::

    %[WIDTH]PROPERTY[(TITLE)][{SUMMARY-TYPES}]

Except for the precent sign and the property name,all items are
optional.The individual parts have the following meaning:

``WIDTH``
                 An integer specifying the width of the column.If omitted,the width
    is determined automatically.

``property``
                    The property that should be edited in this column.Special
    properties representing meta data are allowed here as well.

``TITLE``
                 The header text for the column.If omitted,the property
    named is used.

``SUMMARY-TYPE``
                        The Summary type.If specified,the column values for parent nodes
    are computed from the children.


supported summary types are:

- ``+`` : Sum numbers in this column.

- ``+;%.1f`` : like '+',but format result with '%.1f'.

- ``$`` : Currency,short for '+;%.2f'.

- ``min`` : Smallest number in column.

- ``max`` : Largest number.

- ``mean`` : Arithmetic mean of numbers.

- ``X`` : Checkbox status, '[X]' if all children are '[X]'

- ``X/`` : Checkbox status,'[n/m]'

- ``X%`` : Checkbox status, '[n%]'

- ``:`` : Sum time, HH:MM ,plain numbers are hours.

- ``:min`` : smallest time value in column.

- ``:max`` : Largest  time value.

- ``:mean`` : Arithmetic mean of time values.

- ``@min`` : Minimum age. (in days/hours/mins/seconds).

- ``@max`` : Maximum age. (in days/hours/mins/seconds)

- ``@mean`` : Arithmetic mean of ages (in days/hours/mins/seconds)

- ``est+`` : add low-high estimates.

you can also define custom summary  types by setting
``org-columns-summary-types``.

The ``'est'`` summary type requires further explanation.It is used for
combining estimates,expressed as log-high ranges.For
example,instead of estimating a particular task will take 5
days,you might estimate it as 5-6 days if you're fairly confident
you know how much work is required, or 1-10 days if you do not
really know what needs to be done.Both range average at 5.5
days,but the first represents a more predictable delivery.but the
first represents a more predictable delivery.


When combing a set of such estimates,simply adding the lows and
highs produces an unrealistically wide result.Instead, ``'est+'`` adds
the ``statistical mean and variance`` of the subtasks,generating a
final estimate from the sum. for example,suppose you had ten
tasks,each of which was estimated at 0.5 to 2 days of
work.Straight addition produces an estimate of 5 to 20
days,representing what to expect if everything goes either
extremely well or extremely poorly. In contrast, ``'est+'`` estimates
the full job more realistically,at 10-15 days.

Here is a example for a complete columns definition ,along with
allowed values.

1.5.4 Emacs Org's Column view
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

First press ``C-c C-x C-c`` to show the default column  view.It turn
each outline item into a table row displaying some of its properties.

You can switch the column view *off* and return to the normal view
by pressing *q* while the cursor is on the highlighted entry - but
you can turn the column view on from any location in the buffer.

The first headline is now a row of browsable columns displaying
properties.The first highlighted line of the buffer brielfy tells
you what property is displayed in ecah columns.In this screenshot
it reads:

::

    Item for the headline title
           T for the TODO keyword
           P for the priority cookie
           T for the tags

The default column only show the item,the TODO state,the proority
of the item and its tags,we will see later how to add other
properties of  your own.

This defult setup is driven by the variable
``org-columns-default-format``,which global value is :

::

    #+COULMNS: %25ITEM %TODO %3PRORITY %TAGS

.. table::

    +-----------+------------------------------------------------+
    | ELEMENT   | Description                                    |
    +===========+================================================+
    | %25ITEM   | display the item in a 25-character-width field |
    +-----------+------------------------------------------------+
    | %TODO     | display the TODO state of the item             |
    +-----------+------------------------------------------------+
    | %3PRORITY | display the priority in a 3-chars-width field  |
    +-----------+------------------------------------------------+
    | %TAGS     | display the tags of the entry                  |
    +-----------+------------------------------------------------+

1.5.5 Slight customization of the default column view
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

so now we'd like to customize the column display.

For example,we'd like to change the width of the ``"priority"`` field
and the ``"tags"`` filed in the column:

::

    #+COLUMNS: %25ITEM %5TODO %1PRORITY %10TAGS

`COLUMNS VIEW CASE <~/org/revolt/docs/_rst/_org_demo/_customize_property>`_

The TODO field (%5TODO) is now 5 characters wide,whereas the
priority  and the tags fields are 1 and 10.

Now we'd like to change the title of the columns.For example - and
since we are such **hard workers** - each so-called item is in
fact... a "task":

::

    #+COLUMNS : %25ITEM(Task) %5TODO(To-do) %1PRIORITY %10TAGS

`COLUMNS VIEW CASE <~/org/revolt/docs/_rst/_org_demo/_customize_property>`_

We also add a ``"To-do"`` label for displaying the TODO state for this entry.

1.5.6 Displaying other properties
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

what if you want to display **other properties** in the column view?
For example,we'd like to display the ``SCHEDULED`` property.Then we
need to readfine the global ``#+COLUMNS`` options like this:

::

    #+COLUMNS: %30OTEM %10SCHEDULED %TODO %3PRIOTITY %TAGS

Refresh your Org buffer to take this change into account,then ``C-c C-x C-c`` again on the entry.The column now shows the ``SCHEDULED``
property.

1.5.7 DONE Example of outline item with a SCHEDULE property
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- State "DONE"       from "TODO"       [2019-11-30 Sat 11:36]

::

    CLOSED: [2019-11-30 Sat 11:36] DEADLINE: <2019-11-29 Fri 19:00> SCHEDULED: <2019-11-29 Fri 11:00>
    :PROPERTIES:
    :PRIOTITY: A
    :END:
    :RevoltLogBook:
    - State "DONE"       from "TODO"       [2019-11-30 Sat 11:36]
    :END:

The list of variable properties?Here it is:

.. table::

    +---------------------+----------------------------------------------------------+
    | Variable Properties | Description                                              |
    +=====================+==========================================================+
    | ITEM                | The content of the headline.                             |
    +---------------------+----------------------------------------------------------+
    | TODO                | The TODO keyword of the entry.                           |
    +---------------------+----------------------------------------------------------+
    | TAGS                | The tags defined directly in the headline.               |
    +---------------------+----------------------------------------------------------+
    | ALLTAGS             | All tags,including inherited ones.                       |
    +---------------------+----------------------------------------------------------+
    | PRIORITY            | The priority of the entry,a string with a single letter. |
    +---------------------+----------------------------------------------------------+
    | DEADLINE            | The deadline time string,without the angular brackets.   |
    +---------------------+----------------------------------------------------------+
    | SCHEDULED           | The scheduling time stamp,without the angular brackets.  |
    +---------------------+----------------------------------------------------------+

These are all ``special properties``,but of course you can define your
own properties.

Before going to the ranter complex stuff you can do with your own
properties,we'd like to know how to use different column views for
different subtrees.

1.5.8 Defining a column format for a subtree
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To define a column view for a specific item,just add the special
property: ``:COLUMNS:`` to it.

This view will be used for the entry and its entire subtree - unless
some of its children has its own column view.

::

    **** Top node for columns view
    :PROPERTIES:
    :COLUMNS:  %25ITEM %TAGS %PRIORITY %TODO
    :END:
    See for example this:
    ***** TODO Example1
    ***** TODO Example2
    ***** DONE Example3: [2019-11-29 Fri 21:05]
    :RevoltLogBook:
    - State "DONE"       from "TODO"       [2019-11-29 Fri 21:0

But what if you suddenly prefer ``%TAGS`` to be at the right of ``%TODO``? Put
the cursor in the ``%TAGS`` field and press ``M-<right>``,it will move the
field to the right.

What if you want to make a field larger? Just go to that field and
press ``'>'`` to widen the field (or ``'<'`` narrow it).

If you want to interactively define the column element of a
property,go to its field and press ``'s'``.

so now that we know how to customize the column view for each
entry,it's time to play with user-defined properties.

1.6 DONE Adding summary-types for some properties
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- State "DONE"       from "TODO"       [2019-11-30 Sat 14:31]

Let's define a new entry with its own columns view and a few
properties:

`My Project <~/org/revolt/docs/_rst/_org_demo/_customize_property>`_

but about clock time we can't solve,so we should learn how to sum
clock time,see here `Clock work time <~/org/revolt/docs/_rst/_org_demo/_customize_property>`_.


.. [1] features
