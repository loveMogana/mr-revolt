==============
Org Mode 2 Rst
==============

    :Author: lyps
    :Contact: lyps@github.com
    :Date: 2019/08/13 00:00

-**- mode:org; -**-

1 All
-----

1.1 Heading 1-1
~~~~~~~~~~~~~~~

contents 1-1

1.1.1 Heading 1-1-3
^^^^^^^^^^^^^^^^^^^

contents 1-1-3

1.2 Lists
~~~~~~~~~

1.2.1 Lord of the Rings
^^^^^^^^^^^^^^^^^^^^^^^

1. The attack of the Rohirrim

2. Eowyn's fight with the witch king

   - this was already my favorite scene in the book

   - I really like Miranda Otto.

3. Peter Jackson being shot by Legolas

   - on DVD only

   He makes a really funny face when it happens.

But in the end, no individual scenes matter but the film as a whole.
Important actors in this film are:

Elijah Wood
    He plays Frodo

Sean Austin
    He plays Sam, Frodo's friend.  I still remember
    him very well from his role as Mikey Walsh in The Loonies.

1.3 Special Characters
~~~~~~~~~~~~~~~~~~~~~~

special characters \* asterisk \\ backspace
\.. reStructuredText comment line

1.4 Paragraphs
~~~~~~~~~~~~~~

1.4.1 Verse Block
^^^^^^^^^^^^^^^^^

| Great clouds overhead
|      Tiny black birds rise and fall
|      Snow covers Emacs
| 	 -- AlexSchroeder

1.4.2 Quote block
^^^^^^^^^^^^^^^^^

::

    Everything should be made as simple as possible,
    but not any simpler -- Albert Einstein

    Everything should be made as simple as possible,
    but not any simpler -- Albert Einstein

        Everything should be made as simple as possible,
        but not any simpler -- Albert Einstein

.. note::

    Everything should be made as simple as possible,
    but not any simpler -- Albert Einstein

.. sidebar:: sidebar title

    Everything should be made as simple as possible,
    but not any simpler -- Albert Einstein

1.4.3 Special Block
^^^^^^^^^^^^^^^^^^^

.. caution::

    This is the second line of the first paragraph.

.. note:: This is a note admonition.

    This is the second line of the first paragraph.

1.4.4 Center block
^^^^^^^^^^^^^^^^^^

center block

1.5 Literal Example
~~~~~~~~~~~~~~~~~~~

::
    :name: label

    example

1.6 src block
~~~~~~~~~~~~~

.. code:: common-lisp

    (require 'ox-rst)

1.7 Emphasis and monospace
~~~~~~~~~~~~~~~~~~~~~~~~~~

**bold** *italic* underlined ``verbatim`` ``code`` strike-through

1.8 Subscript and superscript
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

H\ :sub:`2`\O
E = mc\ :sup:`2`\

1.9 Latex fragments
~~~~~~~~~~~~~~~~~~~

If :math:`a^2=b` and :math:`b=2`, then the solution must be
either 

.. math::

    a=+\sqrt{2}

 or 

.. math::

    a=-\sqrt{2}

.



------------

1.10 Comment block
~~~~~~~~~~~~~~~~~~

1.11 Images and Tables
~~~~~~~~~~~~~~~~~~~~~~

.. image:: /_static/aa.jpg
    :alt: alternate text
    :align: right

.. figure:: /_static/aa.jpg
    :alt: alternate text
    :align: center
    :scale: 50%


    image caption

1.12 Tables
~~~~~~~~~~~

1.12.1 One
^^^^^^^^^^

.. table::

    +------+------+------+
    | TOP1 | TOP2 | TOP3 |
    +------+------+------+
    | 1    | 2    | 3    |
    +------+------+------+
    | AAAA | BBBB | CCCC |
    +------+------+------+
    | END1 | END2 | END3 |
    +------+------+------+

1.12.2 Two
^^^^^^^^^^

.. table::

    +------+------+------+
    | TOP1 | TOP2 | TOP3 |
    +======+======+======+
    | 1    | 2    | 3    |
    +------+------+------+
    | AAAA | BBBB | CCCC |
    +------+------+------+
    | END1 | END2 | END3 |
    +------+------+------+

1.12.3 Three
^^^^^^^^^^^^

.. table::

    +------+------+------+
    | TOP1 | TOP2 | TOP3 |
    +======+======+======+
    | 1    | 2    | 3    |
    +------+------+------+
    | AAAA | BBBB | CCCC |
    +------+------+------+
    | END1 | END2 | END3 |
    +------+------+------+

1.12.4 Four
^^^^^^^^^^^

.. table:: caption
    :name: label

    +------+------+------+
    | TOP1 | TOP2 | TOP3 |
    +======+======+======+
    | 1    | 2    | 3    |
    +------+------+------+
    | AAAA | BBBB | CCCC |
    +------+------+------+
    | END1 | END2 | END3 |
    +------+------+------+

1.13 Hyper Links
~~~~~~~~~~~~~~~~

1.13.1 One
^^^^^^^^^^

This is an  _`example`  cross reference target.

Internal cross references, like `example`_

1.13.2 Two
^^^^^^^^^^

Internal cross references, `1.13 Hyper Links`_

.. _customid:

1.13.3 Three
^^^^^^^^^^^^

Internal cross references, headline `customid`_

1.13.4 link table
^^^^^^^^^^^^^^^^^

.. table::
    :name: sampletable

    +---+---+---+
    | a | b | c |
    +---+---+---+
    | 1 | 2 | 3 |
    +---+---+---+

Internal cross references, label `sampletable`_

1.13.5 link image
^^^^^^^^^^^^^^^^^

.. _samplefigure:

.. image:: /_static/aa.jpg

Internal cross references, label `samplefigure`_

1.14 Export reStructedText fragments
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

See also :meth:`mypackage.mymodule.MyClass.mymethod()`.

.. class:: alert

.. class:: alert

2 footnote sample
-----------------

Org mode [1]_  is for keeping notes, maintaining TODO lists, planning projects, and authoring documents with a fast and effective plain-text system.

reStructuredText [2]_  is plaintext that uses simple and intuitive constructs to indicate the structure of a document.


.. [1] org-mode `http://orgmode.org <http://orgmode.org>`_

.. [2] reStructuredText `http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html <http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html>`_
