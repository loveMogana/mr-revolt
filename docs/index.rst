.. revolt documentation master file, created by
   sphinx-quickstart on Wed Nov  6 14:35:54 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to revolt's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents
   :name: mastertoc
   :glob:
   :hidden:
   
   README
   logging
   _rst/_org_mode/*
   _rst/_org_mode/MIT-schema/*
   _rst/_org_mode/Scheme/*
   _rst/_org_demo/*
   _rst/_org_2_rst/*

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
