=============
ORG MODE TAGS
=============

    :Author: lyps

.. contents::

-**- mode:org; -**-

1 Tags
------

Tags are normal words containing letters,numbers, ``"-"`` ,and ``'@'``. Tags
must be preceded and followed by a single colon. e.g.,
*:work:* . Several tags can be specified,as in ``':work:urgent:'`` [1]_ 

Tags by default are in bold face with the same color as the
headline.you may specify special faces for specifics tags using the
variable ``org-tag-faces``, in much the same way as you can for ``TODO``
keywords.

1.1 Tags Inheritance
~~~~~~~~~~~~~~~~~~~~

Tags make use of the hierarchical structure of outline trees. If a
heading has a certain tag.all subheading inherit the tags as
well.For example,in the list:

::

    ;* Meeting with the  French group                                     :work:

    ;** summary by Frank                                            :boss:notes:

    ;'*** Prepare slides for him                                         :action:

you should know the final heading has the tags
``work,boss,notes,action`` even though the final heading is not
explicitly marked with those tags.You can also set tags that all
entries in a file should inherit just as if these tags ware defined
in a hypothetical level zero that surrounds the entries file.

.. code:: text

    #+FILETAGS: :peter:boss:secret:

To limit the inheritance to specific tags,or to turn it off
entirely,use the variables ``org-use-tag-inheritance`` and
``org-tags-exclude-from-inheritance``.

When a headline matches during a tags search while tag inheritance
is turned on,all the sublevels in the same tree-for a simple match
form- match as well.The list of matches may then become very
long. if you just want to see the first tags match in a
subtree,configure the variable ``org-tags-match-list-sublevels``.

Tags inheritance is relevant when the agenda search tries to match a
tag,either in the ``tags`` or ``tags-todo`` agenda types.In other agenda
types, ``org-use-tag-inheritance`` has no effect.Still,you may want to
your tags correctly set in the agenda,so that tags filtering works
fine,with inherited tags.Set ``org-agenda-use-tag-inheritance`` to
control this: the default value includes all agenda types,but
setting this to ``nil`` can really speed up agenda generation.

1.2 Seething Tags
~~~~~~~~~~~~~~~~~

Tags can simply be typed into the buffer at the end of a
headline.After a colon, ``M-TAB`` offers completion on tags.There is
also a special command for inserting tags:

- ``C-c C-q`` *(org-set-tags-command)*

  org supports tag insertion based on a list of tags.By default
  this lit is constructed dynamically,containing all tags,used in
  the buffer.You may also global specify a hard list of tags with
  the variable ``org-tag-alist``.Finally you can set the default tags
  for a given file suing the ``TAGS`` keywords.like:

  ::

      #+TAGS: @work @home @tennisclub
      #+TAGS: laptop car pc sailboat

  if you have globally defined your preferred set of tags using the
  variable ``org-tag-alist`` ,but would like to use a dynamic tag list
  in a specific file,add an empty ``"TAGS"`` keyword to that file:

  ::

      #+TAGS

  If you have a preferred set of tags that you would like to use in
  every file,in addition to those defined on a per-file basic by
  ``"TAGS"`` keyword,then you may specify a list of tags with the
  variable ``org-tag-persistent-alist``. You may turn this off on a
  pre-file basis by adding a ``'STARTUP'`` keyword to that file:

  ::

      #+STARTUP: noptag

  By default Org mode uses the standard minibuffer completion
  facilities for entering tags.However,it also implements
  another,quicker,tag selection method called fast tag
  selection.This allow you to select and deselect tags with just a
  single key press.For this to work well you should assign unique
  letters to most of your commonly used tags.You can do this
  globally by configuring the variable ``org-tag-alist`` in your Emacs
  init file.For example,you may find the need to tag many items in
  different files with ``@home``.In this case you can set something
  like:

  .. code:: common-lisp

      (setq org-tag-alist '(("@work" . ?w) ("@home" . ?h) ("@laptop" . ?l)))

  if the tag is only relevant to the file you are working on,then
  you can instead set the ``"TAGS"`` keyword as:

  .. code:: common-lisp

      #+TAGS: @work(w) @home(h) @tannisclub(t) laptop(l) pc(p)

  now, we test

  `the tags only relevant to the current file <~/org/revolt/source/_org/_org_demo/_customize_tag>`_

  The tags interface shows the available tags in a splash window.If
  you want to start a new line after a specific tag,insert ``'\n'``
  into the tags list.

  `new line add a specify tag <~/org/revolt/source/_org/_org_demo/_customize_tag>`_

  You can also group together tags that are mutually exclusive [2]_  by
  using braces,as in:

  the configuration:

  ::

      #+TAGS: { @work(w) @home(h) @tennisclub(t) } laptop(l) pc(p)

  `tag group show case <~/org/revolt/source/_org/_org_demo/_customize_tag>`_

you indicate that at most one of  *"work"* , *"home"* , *"tennisclub"*
should be selected.Multiple such group are allowed.

To set these mutually exclusive groups in the variable
``org-tags-alist``,you must use the dummy tags ``:startgroup:`` and
``:endgroup`` instead of the braces.Similarly,you can use ``:newline`` to
indicate a line break.The previous example would be set globally by
the following configuration:

.. code:: common-lisp

    (setq org-tag-alist '((:startgroup . nil)
    		      ("@work" . ?w) ("@home"  . ?h)
    		      ("@tennisclub" . ?t)
    		      (:endgroup . nil)
    		      ("laptop" . ?l) ("pc" . ?p)
    		      ))

In this interface ,you can also use the following special keys:

- ``!`` turn off groups of mutually exclusive tags.Use this to (as an
  exception) assign several tags form such a group.

1.3 Tag Hierarchy
~~~~~~~~~~~~~~~~~

Tags can be defined in hierarchies. A tag  can be defined as a group
tag for a set of other tags.The group tag can be seen as the
"broader term" for its set of tags.Defining multiple group tags and
nesting them creates a tag hierarchy.

one use case is to create a  taxonomy [3]_  of terms(tags) that can
be used to classify nodes in a document or set of documents.

when you search for a group tag,it  return matches for all members
in the group and its subgroups. In the agenda view, filtering by a
group tag display or hide headlines tagged with at least one of the
members of the group or any of its subgroups. This makes tag
searches and filters even more flexible.

you can set group tags by using brackets and inserting a colon
between the group tag and its related tags -- beware that all
whitespace are mandatory [4]_  so that org can  parse this line
correctly:

::

    #+TAGS: [ GTD : Control Persp ]

in this example, ``GTD`` is the group  tag and it is related to two
other tags: "Control" , "Persp". Defining "Control" and "Persp" as
group tags creates a hierarchy tags:

::

    #+TAGS: [ GTD : Control Persp ]
    #+TAGS: [Persp: Vision Goal AOF Project]

That can conceptually be seen as a hierarchy of tags:

- GTD

  - Persp

    - Vision

    - Goal

    - AOF

    - Project

  - Control

    - Context

    - Task

`show case <~/org/revolt/source/_org/_org_demo/_customize_tag>`_

You can use the ``:startgrouptag``  , ``:grouptags`` , and ``:endgrouptag``
keyword directly when setting ``org-tag-alist`` directly:

.. code:: common-lisp

    (setq org-tag-alist '(
    		      (:startgrouptag)
    		      ("GTD")
    		      (:grouptags)
    		      ("Control")
    		      ("Persp")
    		      (:endgrouptag)
    		      (:startgrouptag)
    		      ("Control")
    		      (:grouptags)
    		      ("Context")
    		      ("Task")
    		      (:endgrouptag)
    		      ))

The tags in a group can be mutually exclusive if using the same
group syntax as is used for grouping mutually tags together; using
curly brackets.

::

    #+TAGS: { Context  : @Home @Work @Call }

`Hierarchy Tag Groups <~/org/revolt/source/_org/_org_demo/_customize_tag>`_

When setting ``org-tag-alist`` you can use ``:startgroup`` and ``:endgroup``
instead of ``:startgrouptag`` and ``:endgrouptag`` to make the tags mutually
exclusive.

Furthermore,the members of a group tag can also be regular
expression , creating the possibility of a more dynamic and
rule-based tag structure.The regular expressions in the group must
be specified within curly brackets. Here is an expanded example:


::

    #+TAGS; [Vision : {V@.+} ]
    #+TAGS; [Goal: {G@.+} ]
    #+TAGS; [AOF: {AOF@.+} ]
    #+TAGS; [Project: {P@.+} ]


`Regular Expresion tag group <~/org/revolt/source/_org/_org_demo/_customize_tag>`_

Searching for the tag ``"Project"`` now list all tags also including
regular expression matches for ``'P@.+'`` , and similarly for tag
searches on ``"Vision"`` , ``"Goal"`` , and ``"AOF"``. For example,this would
work well for a project tagged with a common project-identifier [5]_ ,
e.g., ``'P@2014_OrgTags'``

1.4 Tag Searches
~~~~~~~~~~~~~~~~

Once a system of tags has been set up, it can be used to collect
related information into special lists.

- ``C-c /m`` or ``C-c \``  ( org-match-sparse-tree)

  With a ``C-u`` prefix argument ,ignore headlines that are not a ``TODO``
  line.

- ``M-x agenda m`` (org-tags-view)

  Create a global list of tag matches from all agenda file.See
  ``Section 10.3.3 {Matching tags and properties}``.

- ``M-x org-agenda M`` (org-tags-view)

  Create a global list of tag matches from all agenda files,but
  check only TODO items and force checking subitems .(see the
  option =org-tags-match-list-sublevels).

  This command all prompt for a match string which alows basic
  Boolean logic like ``'boss+urgent-project1'``, to find entries with
  tags ``'boss'`` and ``'urgent'`` ,but not ``'project'``, or ``'Kathy|Sally'`` to
  find entries which are tagged, like ``'kathy'`` or ``'Sally'`` .

`Regular Expresion tag group <~/org/revolt/source/_org/_org_demo/_customize_tag>`_

The full syntax of the search string is rich and allows also
matching against ``TODO`` keywords,entry levels and properties.For a
complete description with many example,see section 10.3.3 -
Matching tags and properties.


.. [1] anxious for a job

.. [2] just select one form tags group,so it's maturely

.. [3] classify

.. [4] force

.. [5] equals to tags
