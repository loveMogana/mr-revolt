============
Sphinx Usage
============

    :Author: revolt
    :Date: <2019-11-06 Wed 19:36>

.. contents::

-**- mode:org; -**-

1 sphinx blog
-------------

1.1 build project template
~~~~~~~~~~~~~~~~~~~~~~~~~~

- run ``sphinx-quickstart``

1.2 Defining document structure
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- source directory

  - ``conf.py``

  index.rst
      master document-The main function of the
      master document is to serve as a welcome
      page.and to contain the root of "table of
      contents",abbrev is "toctree".This is one of
      the main thing that Sphinx adds to
      ``reStructureText`` , a way to connect multiple
      files to a single hierarchy of documents.

      - reStructureText detectives

        - toctree

          - options

          - content

1.3 Adding the contents
~~~~~~~~~~~~~~~~~~~~~~~

In Sphinx,you can use most feature of standard
``reStructureText``.There are also several features added by
sphinx.For Example,you can add:

- cross-file reference

- view html version

1.4 Running the build
~~~~~~~~~~~~~~~~~~~~~

- build with path
  run ``sphinx-build -b html sourcedir builddir``. *-b* options
  selects a builder,in this example Sphinx will build HTML
  files.

- use script make file
  You can executed by running *make* with the name of the
  builder in source directory.for example: ``make html`` .

- generate PDF documents.
  execute the ``make latexpdf`` command runs the ``Latex builder``
  and readily invokes the ``pdfTex`` toolchain for you.

1.5 Documenting Objects
~~~~~~~~~~~~~~~~~~~~~~~

One of the Sphinx's main objectives is documentation of
objects in any domain.A domain is a collection of object
types that belong together,complete with markup to create
and reference descriptions of these objects.

The most prominent domain [1]_  is the Python domain.For
example,to document Python's built-in function
``enumerate()``,you would add this to one of your source files.

\.. py:function
    enumerate(sequence[, start=0])
    Return an iterator that yield tuples of an index and an item of the

sequence.(and so on).

1.6 Basic Configuration
~~~~~~~~~~~~~~~~~~~~~~~

the `conf.py <source/conf.py>`_ controls how Sphinx processes your document.In
that file,which is executed as a Python source file,you
assign configuration values.

2 Xterm command
---------------

xtrem run new windows:  ``xtrem -e bash -c 'zsh;exec bash'``


.. [1] prominent domain
